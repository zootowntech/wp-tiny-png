<?php
class WP_TinyPNG
{

    private $url = 'http://api.tinypng.org/api/shrink';
    private $key = null;
    private $latestResult = null;
    private $resultJSON = null;

    /**
     * Constructor
     * @param strong $key API key for all requests
     */
    public function __construct($key)
    {
        $this->key = $key;
    }

    /**
     * Send image shrink request
     * @param  string $file path to file to shrink
     * 
     */
    public function shrink($file)
    {
        
        if (file_exists($file) === false) {
            $result = new WP_Error('no_file', 'File does not exist');
        }
        $args = array(
        'timeout' => 10000,
        //works without header info
        //'headers' => array('Authorization' => 'Basic ' . base64_encode( "api:$this->key" )),
        'body' => file_get_contents($file)
        );
        
        $result = wp_remote_post($this->url, $args);
        $this->setResult($result);
        
        if (!is_wp_error($result)) {
            $pngStr = json_encode($result); //turns result into json string
            $pngJson = json_decode($pngStr);//gets result as an array
            $this->setResultJSON($pngJson);
        }
    }


    /**
     * Return API response object
     * @return object. Might be WP_Error
     */
    public function getResult() {
        return $this->latestResult;
    }

    /*
     * Return API response as JSON
     * @return string. Might be WP_Error
     */
    public function getResultJson() {
        return $this->resultJSON;
    }

    /*
     * Returns response code from latest response
     */
    public function getResponseCode(){
        return $this->resultJSON->{'response'}->{'code'};
    }
    /**
     * Process the latest result. If wp-error return error message, 
     * else if bad response return api error code and error message
     * else: all good 
     * @param  string $file name of original file
     * @return string with message displaying error or success message
     */
    public function processResult($oldfile){
        $result = $this->latestResult;
        //wp remote get error 
        if (is_wp_error($result)) {
            $error_string = $result->get_error_message();
            $msg =  "<div class='updated settings-error'> $error_string</div>";
        }else{
            
            $pngJson = $this->getResultJson();
            $jsonBody =  json_decode($pngJson->{'body'}); //get body of json as an array

            //if response not OK then print error
            if ($this->getResponseCode() != 200) {
                $error_string = $jsonBody->{'code'}. ' : ' . $jsonBody->{'message'};
                $msg "<div class='updated settings-error'> $error_string</div>";
            }else{
            //all is good, so get file and save
                $url = $jsonBody->{'output'}->{'url'};

                //changing file name to have -tiny.png suffix
                $oldfileSplit = preg_split('/.png$/', $oldfile);
                $oldfile_no_ext = $oldfileSplit[0];
                $newfilepath = $oldfile_no_ext.'-tiny.png';
                $filenameOnly = basename($newfilepath);

                //getting file from api response url
                $res = wp_remote_get($url);
                $data = $res['body'];

                //save file
                $fp = fopen($newfilepath, "w");
                fwrite($fp, $data);
                fclose($fp);
                
                //adding to media library
                $file_array['name'] = $filenameOnly;
                $file_array['tmp_name'] = $newfilepath;
                $id = $id = media_handle_sideload( $file_array, 0 );

                $msg = "<div class='updated settings-error'> Successfully shrunk and saved <strong>$filenameOnly</strong>.</div>";
            }
        }
        return $msg;
    }

    private function setResult($result){
        $this->latestResult = $result;
    }

    private function setResultJSON($json){
        $this->resultJSON = $json;
    }
    
}


?>