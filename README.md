# README #

##Tiny PNG Wordpress Plugin

A Wordpress plugin [(available here)](https://wordpress.org/plugins/compress-png-for-wp/). This plugin will compress PNG files either when adding new images or existing images in the Wordpress media library. It uses the [Tiny PNG](https://tinypng.com/) API to compress images.