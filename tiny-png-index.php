<?php

/*
 *	Plugin Name: Tiny PNG
 *	Plugin URI: http://www.zootowntech.com/wordpress-plugins/tiny-png
 *	Description: Test TinyPng
 *	Version: 1.0
 *	Author: Scott LaForest
 *	Author URI: http://www.zootowntech.com
 *	License: GPLv2
 *
*/
	register_activation_hook(__FILE__, 'tiny_png_activation');
	function tiny_png_activation(){
		
	}
	

	$added_files_dir = WP_CONTENT_DIR."/uploads/csv-me/";
	add_action( 'admin_menu', 'tiny_png_create_menu' );
	
	function tiny_png_create_menu() {
		
		//create a submenu under Tools
		add_options_page( 'Tiny Png', 'Tiny Png', 'manage_options', __FILE__, 'tiny_png_options_page' );
		
	}
	/
	function tiny_png_options_page(){
		require( 'tiny-png-functions.php' );
		require( 'wp-tiny-png.php' );
		if( !current_user_can( 'manage_options' ) ) {

			wp_die( 'You do not have sufficient permissions to access this page.' );

		}
	
		if(isset($_POST['shrink']))
		{
			foreach ($_POST['shrink'] as $value) {
			
				$message = shrinkAndSaveFile($value);
				echo $message;
				
			}
		}
		
		require( 'tiny-png-options-page.php' );


	
	}

	
	






?>