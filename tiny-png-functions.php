<?php


function get_images_from_media_library() {
    //only gets images that are 'attached' to a post or page
    $args = array(
        'post_type' => 'attachment',
        'post_mime_type' =>'image',
        'post_status' => 'inherit',
        //'posts_per_page' => 10,
        //'orderby' => 'rand'
    );
    $query_images = new WP_Query( $args );
    
    
    $images = array();
    foreach ( $query_images->posts as $image) {
        $fullpath = $image->guid;
        //get only folder and filename after wp-content
        $matches = preg_split('/wp-content/', $fullpath);
        $filepath = WP_CONTENT_DIR;
        $filepath .= $matches[1];
        $filetype = wp_check_filetype($filepath);
        //only want png files
        if ($filetype['ext'] == 'png') {
            $images[]= $filepath;
        }
        
    }
    return $images;
}

function shrinkAndSaveFile($oldfile){
    $myTinyPngKey = 'DcD1AE3NhCARYsKdECPQdIbHNtuuZLL4';
    
    $wpTinyPng = new WP_TinyPNG($myTinyPngKey);
    $wpTinyPng->shrink($oldfile);
    $pngResult = $wpTinyPng->getResult();
    $responseMsg = $wpTinyPng->processResult($oldfile);
    return $responseMsg;
}


?>